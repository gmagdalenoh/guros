# Guros Sr. Fullstack Engineer test
This is a basic App to detect if an array of dna is mutant or not:
- upload
- read metrics from mongodb


# Environment Variables
This app needs the following environment Variables
* `MONGODB_HOST` this is the Mongodb URI string
* `MONGODB_DATABASE` Mongodb database name


# Run file
To run:
* get .env file
* npm install
* npm run dev
* npm run tests

# Test files
To run:
* npm run test

# Futher work
* Create api keys functionality to increase security
* Create a broker message queue for handling millions of requests
* Change Database to SQL to be cheaper than MongoDB
* Adapt Database logic to microservice, this for improving response time

