const indexCtrl = {};
const checkers = require("../libs/Checkers");
const registers = require("../libs/Registers");

indexCtrl.renderIndex = (req, res) => {
    res.send('Hello World');
};

indexCtrl.verifyMutation = async(req, res) => {
    const { dna } = req.body;
    if (!dna) {
        //NOTE: Here we can handle if the dna is NxN letters or other validations (I did not make it beacause dont ask it).
        res.send('Error with format, dna key not found.', 400);
        return;
    }

    let hasMutation = checkers.hasMutation(dna); // Is DNA mutation?

    await registers.registerValidation(hasMutation); // Save record of DNA Mutation analysis result, this is not a promise call intended

    if (hasMutation) {
        res.send('Has Mutation: ' + hasMutation);
    } else if (!hasMutation) {
        res.send('Does not Has Mutation: ' + hasMutation, 403);
    } else {
        res.send('Internal Error', 400);
    }

};

indexCtrl.getStats = async(req, res) => {
    analysisStats = await registers.getAnalysisStats();
    res.send(analysisStats);
};

module.exports = indexCtrl;