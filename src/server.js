const express = require("express");
const path = require("path");
const methodOverride = require("method-override");
const morgan = require("morgan");
const mongoose = require("mongoose");
var cors = require('cors');
const helmet = require('helmet');

// Initializations
const app = express();

// settings
app.set("port", process.env.PORT || 4000);
app.use(express.json());

// middlewares
app.use(cors());
/**
 * app.use(cors({
    origin: 'https://www.guros.com'
  }));
 * We can use CORS to limit the access from selected urls
 */
app.use(helmet()); // Adding Helmet to enhance API's security. We could use more improvements from the server side, even dns side like CloudFlare solution but I don't have time to implement it, sorry.
app.use(morgan("dev"));
app.use(express.urlencoded({ extended: false }));
app.use(methodOverride("_method"));

// routes
app.use(require("./routes/index.routes"));

// static files
app.use(express.static(path.join(__dirname, "public")));

module.exports = app;