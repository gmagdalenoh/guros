const checkers = require('../libs/Checkers');

test('Check DNA ["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"] to be true', () => {
    expect(checkers.hasMutation(["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"])).toBe(true);
});

test('Check DNA ["ATGCGA", "CAGTGC", "TTATTT", "AGACGG", "GCGTCA", "TCACTG"] to be false', () => {
    expect(checkers.hasMutation(["ATGCGA", "CAGTGC", "TTATTT", "AGACGG", "GCGTCA", "TCACTG"])).toBe(false);
});

test('Check DNA ["AAAAAA", "AAAAAA", "AAAAAA", "AAAAAA", "AAAAAA", "AAAAAA"] to be true', () => {
    expect(checkers.hasMutation(["AAAAAA", "AAAAAA", "AAAAAA", "AAAAAA", "AAAAAA", "AAAAAA"])).toBe(true);
});

test('Check DNA ["ABABAB", "ABABAB", "ABABAB", "ABABAB", "ABABAB", "ABABAB"] to be true', () => {
    expect(checkers.hasMutation(["ABABAB", "ABABAB", "ABABAB", "ABABAB", "ABABAB", "ABABAB"])).toBe(true);
});

test('Check DNA ["ABABAB", "BABABA", "ABABAB", "BABABA", "ABABAB", "BABABA"] to be true', () => {
    expect(checkers.hasMutation(["ABABAB", "BABABA", "ABABAB", "BABABA", "ABABAB", "BABABA"])).toBe(true);
});