/** 
 * This file has the logic to read and write data from the Database.
 *
 * It works by reading the data and procesing the data locally instead of the database.
 *
 * @link   jerry2157.com/resume
 * @file   This files defines the logic for reading the database.
 * @author Gerardo Magdaleno.
 * @since  23.12.21
 */

const DNARecord = require("../models/DNARecord");

/**
 * @param {Boolean} analysisResult The result of the analysis
 * @returns {Void}
 */
async function registerValidation(analysisResult) {
    //NOTE: THIS IS AN ACID DB TRANSACTION METHOD
    //2ND NOTE: We could make it a microservice to avoid using a promise
    const newDNARecord = new DNARecord({ isMutation: analysisResult });
    await newDNARecord.save();
}

/**
 * @returns {number|Array} analysisStats The result of the analysis
 */
async function getAnalysisStats() {
    var analysisStats = {
        count_mutations: 0,
        count_no_mutation: 0,
        ratio: 1
    }
    const dnaRecords = await DNARecord.find()
        .sort({ date: "desc" })
        .lean();
    if (dnaRecords) {
        analysisStats.count_mutations = dnaRecords.filter(obj => {
            return obj.isMutation === true
        }).length
        analysisStats.count_no_mutation = dnaRecords.filter(obj => {
            return obj.isMutation === false
        }).length
        analysisStats.ratio = getRatio(analysisStats.count_mutations, analysisStats.count_no_mutation)
    } else {
        analysisStats = {
            error: "no data still"
        }
    }
    return analysisStats;
}

function getRatio(aNumber, bNumber) {
    return ((100 * aNumber) / bNumber) / 100;
}

module.exports = { registerValidation, getAnalysisStats };