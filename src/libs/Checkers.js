/** 
 * This file has the logic to now if an DNA received has mutation.
 *
 * A mutation exists if MORE THAN ONE sequence of four identical letters is found, obliquely, horizontally or vertically.
 *
 * @link   jerry2157.com/resume
 * @file   This files defines the logic of checking if a dna has mutation.
 * @author Gerardo Magdaleno.
 * @since  23.12.21
 */


/**
 * @param {string|Array} dna The string
 * @returns {Boolean} A mutation exists if MORE THAN ONE sequence of four identical letters is found, obliquely, horizontally or vertically.
 */
function hasMutation(dna) {
    var isMutation = false;
    let totalMutations = 0;

    for (let i = 0; i < dna.length; i++) {
        const element = dna[i];
        if (element.includes('AAAA') || element.includes('TTTT') || element.includes('CCCC') || element.includes('GGGG')) { //IS MUTATION HORUZONTALLY?
            totalMutations += 1;
        }
        for (let j = 0; j < element.length; j++) {
            try {
                let extractVertically = dna[i][j] + dna[i + 1][j] + dna[i + 2][j] + dna[i + 3][j]
                let extractObliquallyRight = dna[i][j] + dna[i + 1][j + 1] + dna[i + 2][j + 2] + dna[i + 3][j + 3]
                let extractObliquallyLeft = dna[i][j] + dna[i + 1][j - 1] + dna[i + 2][j - 2] + dna[i + 3][j - 3]
                if (extractVertically.includes('AAAA') || extractVertically.includes('TTTT') || extractVertically.includes('CCCC') || extractVertically.includes('GGGG')) { //IS MUTATION VERTICALLY?
                    totalMutations += 1;
                } else if (extractObliquallyRight.includes('AAAA') || extractObliquallyRight.includes('TTTT') || extractObliquallyRight.includes('CCCC') || extractObliquallyRight.includes('GGGG')) { //IS MUTATION OBLIQUALLY RIGHT?
                    totalMutations += 1;
                } else if (extractObliquallyLeft.includes('AAAA') || extractObliquallyLeft.includes('TTTT') || extractObliquallyLeft.includes('CCCC') || extractObliquallyLeft.includes('GGGG')) { //IS MUTATION OBLIQUALLY LEFT?
                    totalMutations += 1;
                } else {

                }
            } catch (e) {}
        }
    }

    if (totalMutations >= 2) { //if MORE THAN ONE sequence of four identical letters is found
        isMutation = true;
    }

    return isMutation;
}

module.exports = { hasMutation };