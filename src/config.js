module.exports = {
    URL: process.env.URL || 'http://localhost:4005',
    PORT: process.env.PORT || 4000,
    MONGODB_HOST: process.env.MONGODB_HOST || 'mongodb://localhost',
    MONGODB_DATABASE: process.env.MONGODB_DATABASE || 'dna_register'
}