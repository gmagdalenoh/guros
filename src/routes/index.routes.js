const express = require("express");
const router = express.Router();

// Controllers
const { renderIndex, verifyMutation, getStats } = require("../controllers/index.controller");

router.get("/", renderIndex);

router.post("/mutation", verifyMutation);

router.get("/stats", getStats);

module.exports = router;