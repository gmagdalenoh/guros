const { Schema, model } = require("mongoose");

const DNARecordSchema = new Schema({
    isMutation: {
        type: Boolean,
        default: true
    }
}, {
    timestamps: true
});

module.exports = model("DNARecord", DNARecordSchema);